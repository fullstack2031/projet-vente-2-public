import { useSelector } from "react-redux"

export default function ListFruits(){
const mesFruits=useSelector(state=>state.panier)
return(<div>
<h1>liste fruits</h1>
<ul>
{mesFruits.map((f,index)=><li key={index}>{f.nom}</li>)}
</ul>
</div>)
}